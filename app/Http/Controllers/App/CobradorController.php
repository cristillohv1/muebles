<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Cobrador;
use App\Models\CobradorClientes;
use App\Models\Cliente;
use App\Models\Pago;
use App\Models\Venta;
use App\Models\ClienteEstadoCuenta;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;


class CobradorController extends Controller
{
    public function get_cobradores()
    {
        $cobradores = Cobrador::with('clientes')->get();
        \Log::info('SE EJECUTA GET_COBRADORES');

        return response(['cobradores' => $cobradores], 200);
    }


    public function get_data_cobrador(Request $request)
    {
        $cobrador = Cobrador::with('clientes')->where('id', $request->input('cobrador_id'))->first();
        \Log::info('SE EJECUTA GET_DATA_COBRADORES');

        return response(['cobrador' => $cobrador], 200);
    }


    public function sincronizar(Request $request){

        \Log::info('request', $request->input('lista_pagos'));

        try{

            $array_abonos = $request->input('lista_abonos');
            $array_pagos = $request->input('lista_pagos');

            \Log::info('Lista de pagos',[$array_pagos]);

            foreach ($array_pagos as $value)
            {
                $pago_registrado = Pago::where('id', $value['id'])->first();
                if($pago_registrado != null){
                    $pago_registrado->venta_id = $value['venta_id'];
                    $pago_registrado->fecha = $value['fecha'];
                    $pago_registrado->cantidad = $value['cantidad'];
                    $pago_registrado->cobrador_id = $value['cobrador_id'];
                    $pago_registrado->estatus = $value['estatus'];
                    $pago_registrado->save();

                    if($value['liquidacion_compra'] == 'true'){
                        $venta = Venta::where('id', $value['venta_id'])->first();
                        $venta->estatus = 'pagada';
                        $venta->save();
                    }

                }else{
                    $pago = new Pago();
                    $pago->venta_id = $value['venta_id'];
                    $pago->fecha = $value['fecha'];
                    $pago->cantidad = $value['cantidad'];
                    $pago->cobrador_id = $value['cobrador_id'];
                    $pago->estatus = $value['estatus'];
                    $pago->save();

                    if($value['liquidacion_compra'] == 'true'){
                        $venta = Venta::where('id', $value['venta_id'])->first();
                        $venta->estatus = 'pagada';
                        $venta->save();
                    }
                }
            }

            foreach ($array_abonos as $value)
            {
                $abono = new ClienteEstadoCuenta();
                $abono->cliente_id = $value['cliente_id'];
                $abono->fecha = $value['fecha'];
                $abono->saldo_anterior = $value['saldo_anterior'];
                $abono->cantidad_pagada = $value['cantidad_pagada'];
                $abono->saldo_actual = $value['saldo_actual'];
                $abono->save();
            }

            return response()->json('Success', 200);

        }catch (\Exception $e){
            $data = array(
                'estatus' => 500,
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace(),
            );
            return response()->json($data, 500);
        }

    }


    public function registro_pagos()
    {
        //SE OBTIENE LA FECHA ACTUAL
        $now = Carbon::now()->subHours(6);
        $fecha_actual = $now->toDateTimeString();

        //LA FECHA DE VENCIMIENTO ES EL DIA ANTERIOR A LA FECHA ACTUAL
        $date = $now->subDays(1);
        $fecha_vencimiento = $date->toDateTimeString();
        \Log::info($fecha_vencimiento);

        //SE OBTIENE LA LISTA DE LOS PAGOS CON ESTATUS PENDIENTE
        $pagos = Pago::where('estatus', 'Pendiente')->get();
        
        foreach($pagos as $pago){

            //SÍ LA FECHA DE VENCIMIENTO ES IGUAL O MAYOR AL DIA DE PAGO, PASA A ESTATUS NO PAGADO
            if( Carbon::parse($fecha_vencimiento) >= Carbon::parse($pago->fecha)){
                $pago->estatus = 'no pagado';
                $pago->save();

                $new_fecha = Carbon::parse($pago->fecha);
                $fecha_nuevo_pago = $new_fecha->addDays(7);

                //POR CADA PAGO EN ESTATOS NO PAGADO, SE GENERA UN NUEVO PAGO
                $nuevo_pago = new Pago();
                $nuevo_pago->venta_id = $pago->venta_id;
                $nuevo_pago->fecha = $fecha_nuevo_pago->toDateTimeString();
                $nuevo_pago->cantidad = $pago->cantidad;
                $nuevo_pago->cobrador_id = 0;
                $nuevo_pago->estatus = 'Pendiente';
                $nuevo_pago->save();
            }
        }

        //SE OBTIENE EL LISTADO DE CLIENTES QUE TENGAN COMPRAS CON ESTATUS PENDIENTE
        $clientes = Cliente::whereHas('compras', function($q){
            $q->where('estatus', '=', 'Pendiente');
        })->get();
        
        
        //SE ITERA EL LISTADO DE CLIENTES
        foreach ($clientes as $cliente) {
            $total_compras = 0;
            $cantidad_abono = 0;
            $total_abonado = 0;

            //POR CADA COMPRA CON ESTATUS PENDIENTE SE SUMAN $100 A LA CANTIDAD DEL PAGO
            foreach($cliente->compras->where('estatus', 'Pendiente') as $compra){
                $cantidad_abono += 100;
                $total_compras = $total_compras + $compra->total;
                $pagos = Pago::where([['venta_id', $compra->id], ['estatus', 'pagado']])->get();

                foreach($pagos as $pago_registrado){
                    $total_abonado += $pago_registrado->cantidad;
                }
            }
            
            //SE OBTIENE LA PRIMER VENTA CON ESTATUS PENDIENTE
            $venta = $cliente->compras->where('estatus', 'Pendiente')->first();
            
            if($venta != null){
                
                $pago = Pago::where([['venta_id', $venta->id],['estatus', 'Pendiente']])->first();
                
                //SE OBTIENE EL ULTIMO PAGO
                $ultimo_pago = Pago::where('venta_id', $venta->id)->get()->last();
                $fecha_pago = $now->addDays(7);
                
                if($ultimo_pago != null){
                    $fecha_pago = Carbon::parse($ultimo_pago->fecha)->addDays(7);
                }
                
                //EN CASO DE NO TENER PAGOS CON ESTATUS PENDIENTE SE REGISTRA UNO NUEVO, PARA DENTRO DE 1 SEMANA
                if($pago == null){
                    $nuevo_pago = new Pago();
                    $nuevo_pago->venta_id = $venta->id;
                    $nuevo_pago->fecha = $fecha_pago->toDateTimeString();
                    $nuevo_pago->cantidad = $cantidad_abono;
                    $nuevo_pago->cobrador_id = 0;
                    $nuevo_pago->estatus = 'Pendiente';
                    $nuevo_pago->save();
                }else{
                    $pago->cantidad = $cantidad_abono;
                    $pago->save();
                }
                
                \Log::info('noveno');
            }

            
            //ASEGURAR QUE EL CLIENTE CUENTE CON UN ESTADO DE CUENTA REGISTRADO
            $estado_cuenta = ClienteEstadoCuenta::where('cliente_id', $cliente->id)->get()->last();

            if($estado_cuenta == null){
                $estado_cuenta = new ClienteEstadoCuenta();
                $estado_cuenta->cliente_id = $cliente->id;
                $estado_cuenta->fecha = $fecha_actual;
                $estado_cuenta->saldo_anterior = $total_compras;
                $estado_cuenta->cantidad_pagada = 0;
                $estado_cuenta->saldo_actual = $total_compras;
                $estado_cuenta->save();
                
            }else{
                //EN CASO DE QUE SE HAYA AGREGADO UNA NUEVA VENTA AL CLIENTE, SE RECALCULA EL SALDO (DEUDA)
                $nuevo_saldo = $total_compras - $total_abonado;
                $estado_cuenta->saldo_actual = $nuevo_saldo;
                $estado_cuenta->save();
            }
        }

        return response()->json(['estatus' => 'success'], 200);
    }
}
