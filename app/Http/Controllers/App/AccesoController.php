<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use App\Models\Cobrador;
use App\Models\Administrador;
use App\Models\CobradorClientes;
use App\Models\Cliente;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;


class AccesoController extends Controller
{

    public function login(Request $request)
    {

        $usuario = $request->input('usuario');
        $password = $request->input('password');

		$credentials = array('usuario' => $usuario, 'password' => $password);

        $administrador = Administrador::where('usuario', $usuario)->first();
        $cobrador = Cobrador::where('usuario', $usuario)->first();

        if($administrador != null){

            if (Auth::guard('admin')->attempt($credentials)) {

                $administrador->tokens()->delete();
                $token = $administrador->createToken('APP')->plainTextToken;
                $administrador->remember_token = $token;
                $administrador->save();

                return response()->json(['admin' => $administrador], Response::HTTP_OK);
            }else{
                return response(['error' => 'Contraseña incorrecta'], Response::HTTP_BAD_REQUEST);
            }

        }else if($cobrador != null){

            if (Auth::guard('cobrador')->attempt($credentials)) {

                $cobrador->tokens()->delete();
                $token = $cobrador->createToken('APP')->plainTextToken;
                $cobrador->remember_token = $token;
                $cobrador->save();

                return response()->json(['cobrador' => $cobrador], Response::HTTP_OK);
            }else{
                return response(['error' => 'Contraseña incorrecta'], Response::HTTP_BAD_REQUEST);
            }

        }else{
            return response(['error' => 'No se encontró el usuario'], Response::HTTP_BAD_REQUEST);
        }

    }


    public function create()
    {
        /*$cobrador = new Cobrador;
        $cobrador->usuario = 'Cristian';
        $cobrador->nombre = 'Cristian Leyva';
        $cobrador->telefono = '4761072633';
        $cobrador->calle = 'José Ma. Morelos #117';
        $cobrador->colonia = 'Centro';
        $cobrador->cp = '36400';
        $cobrador->ciudad = 'San Francisco';
        $cobrador->telefono = '4761072633';
        $cobrador->estado = 'Jalisco';
        $cobrador->password = bcrypt('admin');
        $cobrador->estatus = 'activo';
        $cobrador->save();*/

        $admin = new Administrador;
        $admin->usuario = 'Admin';
        $admin->password = bcrypt('admin');
        $admin->rol_id = 1;
        $admin->save();

        return response(['estatus' => 'success'], 200);
    }







}
