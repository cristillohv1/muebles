<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Pago extends Model
{
    protected $table = 'pagos';


    public function venta()
    {
        return $this->belongsTo(Venta::class,'venta_id')->with('cliente', 'venta_productos');
    }


    /*public function cliente()
    {
        return $this->hasMany(Cliente::class, 'cobrador_id');
    }*/

}
