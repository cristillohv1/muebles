<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class CobradorClientes extends Model
{
    protected $table = 'cobrador_clientes';

    public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'cliente_id')
        ->with('estado_cuenta')
        ->with('compras');
	}

}
