<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Cliente extends Model
{
    protected $table = 'clientes';



    public function estado_cuenta()
	{
		return $this->hasMany(ClienteEstadoCuenta::class, 'cliente_id');
	}


    public function compras()
	{
		return $this->hasMany(Venta::class, 'cliente_id')->with('pagos','venta_productos')->where('estatus','like', 'pendiente');
	}


}
