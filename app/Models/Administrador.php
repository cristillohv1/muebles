<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;


class Administrador extends Authenticatable
{
    use Notifiable, HasApiTokens;
    protected $table = 'administradores';

}
