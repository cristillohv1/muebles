<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;


class Cobrador extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'cobradores';

    protected $hidden = [
        'password'
    ];

    public function clientes()
	{
		return $this->hasMany(CobradorClientes::class, 'cobrador_id')->with('cliente');
	}

    public function pagos()
    {
        return $this->hasMany(Pago::class, 'cobrador_id')->with('venta');
    }
}
