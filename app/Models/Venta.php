<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Venta extends Model
{
    protected $table = 'ventas';


    public function venta_productos()
	{
		return $this->hasMany(VentaProducto::class, 'venta_id')->with('producto');
	}

    public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'cliente_id');
	}

    public function pagos()
    {
        return $this->hasMany(Pago::class, 'venta_id')->where('estatus','like', 'pendiente');
    }

}
