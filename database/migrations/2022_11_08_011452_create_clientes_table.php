<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('calle');
            $table->string('colonia');
            $table->string('cp');
            $table->string('ciudad');
            $table->string('estado');
            $table->string('aval_nombre');
            $table->string('aval_telefono');
            $table->string('aval_calle');
            $table->string('aval_colonia');
            $table->string('aval_cp');
            $table->string('aval_ciudad');
            $table->string('aval_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
