<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteEstadoCuentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente_estado_cuenta', function (Blueprint $table) {
            $table->id();
            $table->integer('cliente_id');
            $table->string('fecha');
            $table->decimal('saldo_anterior');
            $table->decimal('cantidad_pagada');
            $table->decimal('saldo_actual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente_estado_cuenta');
    }
}
