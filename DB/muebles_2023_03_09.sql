-- MySQL dump 10.13  Distrib 8.0.32, for macos12.6 (arm64)
--
-- Host: localhost    Database: muebles
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administradores`
--

DROP TABLE IF EXISTS `administradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `administradores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol_id` int NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administradores`
--

LOCK TABLES `administradores` WRITE;
/*!40000 ALTER TABLE `administradores` DISABLE KEYS */;
INSERT INTO `administradores` VALUES (1,'Hector','$2y$10$7XHitSju6eYjWQovVG6phOibipTz8Hrv.YLfxuwynIzLJgacb.2xO',1,'256|YXaidI6OAxeeVLi7k6jvynRrHOIOvO8akWLzohmh',NULL,'2023-01-03 01:46:30');
/*!40000 ALTER TABLE `administradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente_estado_cuenta`
--

DROP TABLE IF EXISTS `cliente_estado_cuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente_estado_cuenta` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int NOT NULL,
  `fecha` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo_anterior` decimal(8,2) NOT NULL,
  `cantidad_pagada` decimal(8,2) NOT NULL,
  `saldo_actual` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente_estado_cuenta`
--

LOCK TABLES `cliente_estado_cuenta` WRITE;
/*!40000 ALTER TABLE `cliente_estado_cuenta` DISABLE KEYS */;
INSERT INTO `cliente_estado_cuenta` VALUES (1,1,'2022-02-02 19:07:00',5000.00,100.00,400.00,NULL,'2023-01-02 22:24:08'),(2,2,'2022-02-09 19:07:00',2000.00,100.00,1650.00,NULL,'2023-01-02 22:24:08'),(3,1,'2022-02-16 19:07:00',4900.00,100.00,4800.00,NULL,NULL),(4,1,'2022-12-22 01:33',4600.00,200.00,4400.00,'2022-12-22 13:38:28','2022-12-22 13:38:28'),(5,1,'2022-12-22 01:37',4600.00,200.00,4400.00,'2022-12-22 13:38:28','2022-12-22 13:38:28'),(6,2,'2022-12-22 01:38',1900.00,150.00,1750.00,'2022-12-22 13:38:28','2022-12-22 13:38:28'),(7,1,'2022-12-22 01:39',4400.00,130.00,4270.00,'2022-12-22 13:40:35','2022-12-22 13:40:35'),(8,1,'2022-12-22 13:34',4270.00,4270.00,400.00,'2022-12-23 01:35:25','2023-01-03 00:20:11'),(9,2,'2022-12-23 14:43',1750.00,400.00,1350.00,'2022-12-24 02:46:34','2022-12-24 02:46:34'),(10,2,'2022-12-23 15:27',1350.00,400.00,1650.00,'2022-12-24 03:35:21','2023-01-03 00:20:11'),(11,4,'2023-01-01 11:23:49',5100.00,0.00,2100.00,'2023-01-01 23:23:49','2023-01-02 22:24:08'),(12,4,'2023-01-01 18:55',5100.00,100.00,5000.00,'2023-01-02 06:56:56','2023-01-02 06:56:56'),(13,4,'2023-01-01 19:01',5000.00,100.00,4900.00,'2023-01-02 07:01:53','2023-01-02 07:01:53'),(14,4,'2023-01-01 19:04',4900.00,100.00,4800.00,'2023-01-02 07:05:32','2023-01-02 07:05:32'),(15,4,'2023-01-01 19:28',1700.00,100.00,2100.00,'2023-01-02 07:30:06','2023-01-02 23:20:14'),(16,4,'2023-01-02 12:18',2100.00,1000.00,1100.00,'2023-01-03 00:19:42','2023-01-03 00:20:11'),(17,4,'2023-01-01 13:16',1100.00,1100.00,0.00,'2023-01-03 01:17:26','2023-01-03 01:17:26');
/*!40000 ALTER TABLE `cliente_estado_cuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clientes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `colonia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_telefono` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_calle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_colonia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_cp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_ciudad` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `aval_estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Jordan Avila Gomez','Morelos','San Javier','26400','Purisima del Rincon','Guanajuato','Cristian','4761073633','5 de mayo','Centro','36400','San francisco','Guanjuato',NULL,NULL),(2,'Pau Rodriguez','Guerrero','Centro','66400','Purisima del Rincon','Guanajuato','Alan','4761073633','Hidalgo','Centro','36400','León','Guanjuato',NULL,NULL),(3,'Maria Gutierrez','16 de Septiembre','San Jorge','44500','San Diego','Jalisco','Cristo','4761087945','Hospital #144','Del Carmen','38400','Manuel Doblado','Jalisco',NULL,NULL),(4,'Mario Quiroz','16 de Septiembre','San Jorge','44500','San Diego','Jalisco','Cristo','4761087945','Hospital #144','Del Carmen','38400','Manuel Doblado','Jalisco',NULL,NULL),(5,'Cristopher Villareal','Hidalgo','La Estacion','66400','Purisima del Rincon','Guanajuato','Alan','4761073633','Hidalgo','Centro','36400','León','Guanjuato',NULL,NULL),(6,'Test Guzman','Hidalgo','Jalpa','66400','Purisima del Rincon','Guanajuato','Alan','4761073633','Hidalgo','Centro','36400','León','Guanjuato',NULL,NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobrador_clientes`
--

DROP TABLE IF EXISTS `cobrador_clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cobrador_clientes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `cobrador_id` int NOT NULL,
  `cliente_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cobrador_clientes`
--

LOCK TABLES `cobrador_clientes` WRITE;
/*!40000 ALTER TABLE `cobrador_clientes` DISABLE KEYS */;
INSERT INTO `cobrador_clientes` VALUES (1,1,1,NULL,NULL),(2,1,2,NULL,NULL),(3,1,3,NULL,NULL),(4,3,4,NULL,NULL),(5,1,5,NULL,NULL),(6,3,6,NULL,NULL);
/*!40000 ALTER TABLE `cobrador_clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cobradores`
--

DROP TABLE IF EXISTS `cobradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cobradores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `colonia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudad` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `estatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cobradores`
--

LOCK TABLES `cobradores` WRITE;
/*!40000 ALTER TABLE `cobradores` DISABLE KEYS */;
INSERT INTO `cobradores` VALUES (1,'Cris','$2y$10$7XHitSju6eYjWQovVG6phOibipTz8Hrv.YLfxuwynIzLJgacb.2xO','Cristian Leyva','4761072633','José Ma. Morelos #117','Centro','36400','Purisima del Rincón','Guanajuato','activo','258|b1cZ9LLTVY8DIds9rfDrAGye5sLlc9AYg9f680md','2022-11-29 05:38:33','2023-02-04 04:17:51'),(2,'Ruth','$2y$10$zFYBIrXtF5sb04xOIGaMdeIZFjoizg9rHRC1fduA7E55CxaDSa.oC','Ruth López','4761072633','José Ma. Morelos #117','Centro','36400','Purisima del Rincón','Guanajuato','activo',NULL,'2022-11-29 05:40:02','2022-11-29 05:40:02'),(3,'Carlitos','$2y$10$2pZpQHqBm0tE2/Ra.8xfh.LnveLsufiFVARM05XzW5AxF.znvXL4K','Carlos Soto','4761072633','José Ma. Morelos #117','Centro','36400','San Francisco','Jalisco','activo','257|qRaPfMQ3z0v4PlJtnWv2Oo2yf3t8fqbMKcZ9IhGc','2022-11-29 05:41:41','2023-01-03 01:47:13');
/*!40000 ALTER TABLE `cobradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1),(5,'2022_11_08_011427_create_cobradores_table',1),(6,'2022_11_08_011442_create_cobrador_clientes_table',1),(7,'2022_11_08_011452_create_clientes_table',1),(8,'2022_11_08_011502_create_ventas_table',1),(9,'2022_11_08_011510_create_venta_productos_table',1),(10,'2022_11_08_011518_create_pagos_table',1),(11,'2022_11_08_011525_create_productos_table',1),(12,'2022_11_08_011538_create_cliente_estado_cuenta_table',1),(13,'2022_11_08_011550_create_administradores_table',1),(14,'2022_11_08_011558_create_roles_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pagos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `venta_id` int NOT NULL,
  `fecha` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` decimal(8,2) NOT NULL,
  `cobrador_id` int NOT NULL,
  `estatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` VALUES (54,2,'2022-12-18 15:27',100.00,1,'no pagado',NULL,'2022-12-27 02:34:19'),(55,3,'2022-12-17 01:33:00',200.00,1,'no pagado','2022-12-22 13:33:38','2022-12-27 02:34:19'),(56,3,'2022-12-16 01:33:00',200.00,1,'no pagado','2022-12-22 13:38:28','2022-12-27 02:34:19'),(57,3,'2022-12-22 01:37:00',200.00,1,'pagado','2022-12-22 13:38:28','2022-12-22 13:38:28'),(58,2,'2022-12-22 01:38:00',150.00,1,'pagado','2022-12-22 13:38:28','2022-12-22 13:38:28'),(59,3,'2022-12-22 01:39:00',130.00,1,'pagado','2022-12-22 13:40:35','2022-12-22 13:40:35'),(60,1,'2022-12-22 13:34:00',3000.00,1,'pagado','2022-12-23 01:35:25','2022-12-23 01:35:25'),(61,3,'2022-12-22 13:34:00',1270.00,1,'pagado','2022-12-23 01:35:25','2022-12-23 01:35:25'),(62,2,'2022-12-23 15:27',100.00,1,'pagado',NULL,'2022-12-24 03:35:21'),(63,2,'2022-12-26 15:27',100.00,1,'pagado',NULL,'2022-12-24 03:35:21'),(64,3,'2022-12-26 17:48:23',100.00,0,'no pagado','2022-12-27 05:48:23','2023-01-01 00:03:52'),(65,2,'2022-12-26 17:49:22',100.00,0,'no pagado','2022-12-27 05:49:22','2023-01-01 00:03:52'),(66,2,'2023-01-06 12:07:03',100.00,0,'no pagado','2023-01-01 00:07:03','2023-01-01 23:22:44'),(67,3,'2023-01-06 12:07:03',100.00,0,'no pagado','2023-01-01 00:07:03','2023-01-01 23:22:44'),(68,4,'2023-01-06 12:07:03',100.00,0,'no pagado','2023-01-01 00:07:03','2023-01-01 23:22:44'),(69,2,'2023-01-07 11:22:44',100.00,0,'no pagado','2023-01-01 23:22:44','2023-01-01 23:23:49'),(70,3,'2023-01-07 11:22:44',100.00,0,'no pagado','2023-01-01 23:22:44','2023-01-01 23:23:49'),(71,4,'2023-01-07 11:22:44',400.00,0,'pagado','2023-01-01 23:22:44','2023-01-01 23:23:49'),(72,2,'2023-01-07 11:23:49',100.00,0,'pendiente','2023-01-01 23:23:49','2023-01-03 00:20:11'),(73,3,'2023-01-07 11:23:49',100.00,0,'pendiente','2023-01-01 23:23:49','2023-01-03 00:20:11'),(74,4,'2023-01-07 19:01:00',700.00,3,'pagado','2023-01-01 23:23:49','2023-01-03 00:19:42'),(90,5,'2023-01-07 19:01:00',300.00,3,'pagado','2023-01-03 00:19:42','2023-01-03 00:19:42'),(91,5,'2023-01-07 19:01:00',600.00,3,'pagado','2023-01-03 00:20:11','2023-01-03 01:17:26'),(92,2,'2023-02-03 19:01:00',100.00,1,'pendiente','2023-01-03 01:17:26','2023-01-03 01:17:26');
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
INSERT INTO `personal_access_tokens` VALUES (22,'App\\Models\\User',2,'APP','7fb598f4cc6e1adac8e79eaa66294b641b9c0f35f296bd096b8df09b6d5e8cf6','[\"*\"]',NULL,'2022-11-29 05:03:10','2022-11-29 05:03:10'),(38,'App\\Models\\User',1,'APP','00277354012fdf3c7ed76ca01bea6e1dc887a83ba1646665bb0d92ab81bce53d','[\"*\"]','2022-12-07 06:31:13','2022-12-06 05:27:37','2022-12-07 06:31:13'),(256,'App\\Models\\Administrador',1,'APP','95cb3a1119679082070b861551d24e5b66628f39b774a051d06231aae22864a1','[\"*\"]','2023-01-03 01:46:30','2023-01-03 01:46:30','2023-01-03 01:46:30'),(257,'App\\Models\\Cobrador',3,'APP','48fe5a28896e7faf67317a46cb6b8f521d479258157e8aafe960e4541ac5610c','[\"*\"]','2023-01-03 01:50:11','2023-01-03 01:47:13','2023-01-03 01:50:11'),(258,'App\\Models\\Cobrador',1,'APP','364350965dff1aa5bdd25dd79ae1e86645b8da191db2a296a158cb7ae8e1a9d9','[\"*\"]',NULL,'2023-02-04 04:17:51','2023-02-04 04:17:51');
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Sala',1000.00,'Buena calidad',NULL,NULL),(2,'Recamara',500.00,'King size',NULL,NULL),(3,'Ropero',2000.00,'De madera',NULL,NULL);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin',NULL,NULL),(2,'Cobrador',NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rol_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Alan Lopez','ajlj-99@live.com',NULL,'$2y$10$5qE5Cv69anHTVAX3RwceZunhsasBNKFRhhd9j3d48Z9VvzjHOGnEy','38|Pfw3fiubdSnqotbBOOT7FExpLteedY3vmhbP1Ndg',1,'2022-11-26 04:35:59','2022-12-06 05:27:37'),(2,'Jordan Avila Gomez','jordan@gmail.com',NULL,'$2y$10$Pq.BaczyDP9r3ku6LiOEeuFWTvSgIjXqsWktBtYv3f5UH3lrFoKpu','22|L4STnp8w2iMny1bEevNm1qW0qDlB97Ftb5CI4523',2,'2022-11-26 04:36:46','2022-11-29 05:03:10');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta_productos`
--

DROP TABLE IF EXISTS `venta_productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `venta_productos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `venta_id` int NOT NULL,
  `producto_id` int NOT NULL,
  `cantidad` decimal(8,2) NOT NULL,
  `precio_subtotal` decimal(8,2) NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta_productos`
--

LOCK TABLES `venta_productos` WRITE;
/*!40000 ALTER TABLE `venta_productos` DISABLE KEYS */;
INSERT INTO `venta_productos` VALUES (1,1,1,1.00,1000.00,1000.00,NULL,NULL),(2,1,2,1.00,500.00,500.00,NULL,NULL),(3,2,2,1.00,500.00,500.00,NULL,NULL),(4,2,1,1.00,1500.00,1500.00,NULL,NULL),(15,3,3,1.00,2000.00,2000.00,NULL,NULL);
/*!40000 ALTER TABLE `venta_productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ventas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int NOT NULL,
  `fecha` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `descuento` decimal(8,2) NOT NULL,
  `iva` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `cobrador_id` int NOT NULL,
  `estatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_contado` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (1,1,'2022-22-02','3000',0.00,10.00,3000.00,1,'pagada',NULL,NULL,NULL),(2,2,'2022-22-02','2000',0.00,10.00,2000.00,1,'pendiente',NULL,NULL,NULL),(3,1,'2022-22-12','2000',0.00,100.00,2000.00,1,'pendiente',NULL,NULL,NULL),(4,4,'2022-22-12','1000',0.00,100.00,1100.00,1,'pagada',NULL,NULL,'2023-01-03 00:19:42'),(5,4,'2022-22-12','800',0.00,100.00,900.00,1,'pagada',NULL,NULL,'2023-01-03 01:17:26'),(6,4,'2022-22-12','400',0.00,100.00,500.00,1,'pendiente',NULL,NULL,'2023-01-03 01:17:26');
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-09 17:32:07
